# pico-gps-l80

This is a simple project to use the cheap [L80 GPS receiver from Quectel](https://www.quectel.com/wp-content/uploads/2021/03/Quectel_L80_Hardware_Design_V1.5-1.pdf) with the Raspberry Pico.
The connexion is setup through a standard UART serial communication.
The project uses a SSD1306 LCD display for displaying the satellites in view, the present location and received date/time.

# Schema
VCC and V_BCKP are connected.
The two condensators are 47uF and 10nF. The datasheet recommand 10uF and 100nF.
![schema](schema.jpg)

# In operation
![the result](gps-in-operation.jpg)

