import time

# Simple class for decoding GPGSV, GPRMC, GPVTG and GPZDA NMEA messages.
# The add method is used to add byte buffer read from the GPS receiver.
class NMEA():
    
    def __init__(self):
        self.sv = []
        self.svready = False
        self.lat = 0.
        self.lon = 0.
        self.timefix = '00:00:00'
        self.datefix = '00/00/00'
        self.time = '00:00:00'
        self.date = '00/00/00'
        self.datetime=None
        self.heading = 0.
        self.speed = 0.

    def add(self,msg):
        if (msg == None):
            return
        if not self.check(msg):
            return
        elts = msg.decode('ascii').split(',')
        if elts[0] == '$GPGSV':
            self.parse_gpgsv(elts)
        if elts[0] == '$GPRMC':
            self.parse_gprmc(elts)
        if elts[0] == '$GPVTG':
            self.parse_gpvtg(elts)
        if elts[0] == '$GPZDA':
            self.parse_gpzda(elts)
        return elts[0]
    
    def check(self,msg):
        if len(msg)<4:
            return False
        # Should we remove \r\n ?
        last=0
        if msg[-1] == 10 and msg[-2] == 13:
            last=-2
            checksum=msg[last-2:last]
        else:
            checksum=msg[-2:]
        # Do we have $...*XX as expected ?
        if msg[0] != 36 or msg[last-3] != 42:
            return False
        # Compute XOR between $ and *
        x=msg[1]
        for b in msg[2:last-3]:
            x^=b
        # Check computed against expected
        return x == int(checksum.decode('ascii'),16)


    def parse_gpgsv(self,elts):
        # First message, reset and start processing SVs
        if elts[2]=='1':
            self.sv.clear()
            self.svready = False
        # Loop through SVs in this message and may expect more messages
        idx=4
        while (idx+4) < len(elts) and (elts[idx] != '') and (elts[idx+1] != '') and (elts[idx+2] != ''):
            sn=int(elts[idx])
            elev=float(elts[idx+1])
            az=float(elts[idx+2])
            self.sv.append((sn,elev,az))
            idx += 4
        # Last message processed, SV is ready
        if elts[2] == elts[1]:
            self.svready = True

    def parse_gprmc(self,elts):
        if elts[2] == 'A':
            latD = float(elts[3][0:2])
            latM = float(elts[3][2:])
            self.lat = latD+latM/60
            if elts[4] == 'S':
                self.lat = -self.lat
            lonD = float(elts[5][0:3])
            lonM = float(elts[5][3:])
            self.lon = lonD+lonM/60
            if elts[6] == 'W':
                self.lon = -self.lon
            self.timefix = elts[1][0:2]+":"+elts[1][2:4]+":"+elts[1][4:]
            self.datefix = elts[9][0:2]+"/"+elts[9][2:4]+"/20"+elts[9][4:]

    def parse_gpvtg(self,elts):
        self.heading=float(elts[1])
        self.speed=float(elts[7])

    def parse_gpzda(self,elts):
        h=elts[1][0:2]
        m=elts[1][2:4]
        s=elts[1][4:]
        d=elts[2]
        M=elts[3]
        y=elts[4]
        if int(y)>=2022 and int(y)<2100 and int(M)>0 and int(d)>0:
            self.datetime = (int(y),int(M),int(d),int(h),int(m),float(s))
            self.time = h+":"+m+":"+s
            self.date = d+"/"+M+"/"+y[2:] # Keep only YY


