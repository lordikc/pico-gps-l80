from machine import Pin, Timer, ADC,I2C, UART, RTC
import time
import ssd1306
from nmea import NMEA
import math

# Lines for drawing the quadrant    
lines = (
( 32 , 63 , 37 , 63 ),
( 37 , 63 , 42 , 62 ),
( 42 , 62 , 48 , 59 ),
( 48 , 59 , 52 , 56 ),
( 52 , 56 , 56 , 52 ),
( 56 , 52 , 59 , 48 ),
( 59 , 48 , 62 , 42 ),
( 62 , 42 , 63 , 37 ),
( 63 , 37 , 64 , 32 ),
( 64 , 32 , 63 , 26 ),
( 63 , 26 , 62 , 21 ),
( 62 , 21 , 59 , 16 ),
( 59 , 16 , 56 , 11 ),
( 56 , 11 , 52 , 7 ),
( 52 , 7 , 48 , 4 ),
( 48 , 4 , 42 , 1 ),
( 42 , 1 , 37 , 0 ),
( 37 , 0 , 32 , 0 ),
( 32 , 0 , 26 , 0 ),
( 26 , 0 , 21 , 1 ),
( 21 , 1 , 15 , 4 ),
( 15 , 4 , 11 , 7 ),
( 11 , 7 , 7 , 11 ),
( 7 , 11 , 4 , 15 ),
( 4 , 15 , 1 , 21 ),
( 1 , 21 , 0 , 26 ),
( 0 , 26 , 0 , 31 ),
( 0 , 31 , 0 , 37 ),
( 0 , 37 , 1 , 42 ),
( 1 , 42 , 4 , 48 ),
( 4 , 48 , 7 , 52 ),
( 7 , 52 , 11 , 56 ),
( 11 , 56 , 15 , 59 ),
( 15 , 59 , 21 , 62 ),
( 21 , 62 , 26 , 63 ),
( 26 , 63 , 31 , 63 ),
( 9 , 54 , 54 , 9 ),
( 54 , 54 , 9 , 9 ),
( 0 , 32 , 64 , 32 ),
( 32 , 0 , 32 , 64 )
)

# Redraw LCD
def redraw(timer):
    global lcd,nmea
    lcd.fill(0)
    for l in lines:
        lcd.line(l[0],l[1],l[2],l[3],255)
    
    lcd.line(32,32,
             int(32+16*math.sin(math.radians(nmea.heading))),
             int(32-16*math.cos(math.radians(nmea.heading))),255)

    if (nmea.svready):
        for sat in nmea.sv:
            az = float(sat[2])
            el = float(sat[1])
            x = int(32+32*math.sin(math.radians(az))*math.cos(math.radians(el)))
            y = int(32-32*math.cos(math.radians(az))*math.cos(math.radians(el)))
            lcd.line(x-2,y,x+2,y,255)
            lcd.line(x,y-2,x,y+2,255)
            lcd.line(x-2,y-2,x+2,y+2,255)
            lcd.line(x-2,y+2,x+2,y-2,255)

    lcd.text(f'{nmea.lat:.5f}',65,0)
    lcd.text(f'{nmea.lon:.5f}',65,10)
    lcd.text(f'{nmea.speed:.1f}',65,20)
    lcd.text(nmea.date,65,30)
    lcd.text(nmea.time,65,40)
    lcd.show()

if __name__ == "__main__":
    # Setup the LCD display connection through I2C
    i2c = I2C(1, sda=Pin(14), scl=Pin(15))
    lcd = ssd1306.SSD1306_I2C(128, 64, i2c)
    lcd.text('Starting...', 18, 27)
    lcd.show()
    led = Pin("LED", Pin.OUT)
    led.value(1)
    time.sleep(2)
    led.value(0)
    
    # Setup the UART connection to the GPS receiver
    rx = UART(0,9600,bits=8,parity=None,stop=1,tx=Pin(16),rx=Pin(17),timeout=1000)
    nmea = NMEA()
    # Timer for redrawing the LCD every second.
    timer = Timer(mode=Timer.PERIODIC, period=1000, callback=redraw)
    # Enable ZDA
    rx.write(b'$PMTK314,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,0*29\r\n')
    rx.flush()
    rx.write(b'$PMTK314,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,0*29\r\n')
    rx.flush()
    rtc = RTC()
    while True:
        try:
            recv = rx.readline()
            #print(recv)
            if recv == None:
                continue
            cmd = nmea.add(recv)
            # Adjust system time if need be
            if cmd == '$GPZDA' and nmea.datetime != None:
                sec_gps = time.mktime((nmea.datetime[0],
                                      nmea.datetime[1],
                                      nmea.datetime[2],
                                      nmea.datetime[3],
                                      nmea.datetime[4],
                                      int(nmea.datetime[5]),
                                      None,None))
                sec_sys = time.time()
                error = abs(sec_gps - sec_sys)
                # Update system datetime according to GPS datetime
                if (error > 2):
                    rtc.datetime((nmea.datetime[0],
                                  nmea.datetime[1],
                                  nmea.datetime[2],
                                  None,
                                  nmea.datetime[3],
                                  nmea.datetime[4],
                                  int(nmea.datetime[5]),
                                  0))
        except:
            print("Error...")
            pass

